import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';
import { connect } from 'react-redux';
import Layout from '../../src/components/Layout';
import LoadingIndicator from '../../src/components/LoadingIndicator';
import { setProfile, clearProfile } from '../../redux/actions';
import FormInput from '../../src/components/FormInput';
import Swal from 'sweetalert2/dist/sweetalert2';
import styles from '../../src/css/style.scss';

class FormEdit extends PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
        style: {},
        firstName: "",
        lastName: "",
        age: "",
        upload: null,
        uploadSuccess: null,
        isLoading: false,
        message: null
    };
  }

  componentDidMount() {
    const { profile } = this.props;
    console.log(profile);
    let id = JSON.parse(localStorage.getItem('id'));
    this._isMounted = true;
    this.fetchController = new AbortController();
    this.setState({ style: styles });
    this.handleGetData(id, this.fetchController.signal);
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.fetchController.abort();
    this.props.clearProfile();
    localStorage.clear();
  }

  componentDidUpdate(prevProps, prevState) {
    const { profile } = this.props;
    console.log(profile);
    if(!localStorage.getItem('id')){
        window.location.href = '/';
    }
  }

  handleGetData = async (id, signal) => {
    if(!this._isMounted){
      return;
    }

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        signal
      };
      const response = await fetch('https://simple-contact-crud.herokuapp.com/contact/'+id, requestOptions);
      if(!response.ok){
        this.setState({
          message: 'Something went wrong',
          isLoading: false
        })
        return;
      }
      const data = await response.json();
      const result = data || [];
      await this.props.setProfile(result.data).then(async () => {
        this.setState({
            firstName: result.data.firstName,
            lastName: result.data.lastName,
            age: result.data.age,
            isLoading: false
          });
      });
  }

  handleSubmit = async (e) => {
    if(!this._isMounted){
        return;
    }
    
    const signal = this.fetchController.signal;
    e.preventDefault();
    const { 
        firstName,
        lastName,
        age,
        upload
    } = this.state;

    if(firstName === "" || lastName === "" || age === ""){
        Swal.fire({
            title: 'Warning',
            text: 'All field is required.',
            icon: 'warning',
            confirmButtonText: 'Close'
        })
        return;
    } else {
        if(upload && upload[0].type !== "image/jpeg" && upload[0].type !== "image/png" && upload[0].type !== "image/jpg") {
            Swal.fire({
                title: 'Warning',
                text: 'Incorrect image file type.',
                icon: 'warning',
                confirmButtonText: 'Close'
            })
            return;
        } else {
            if(upload && upload[0]){
                const toBase64 = file => new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => resolve(reader.result);
                    reader.onerror = error => reject(error);
                });
                this.setState({ isLoading: true });
                toBase64(upload[0]).then(async (e) => {
                    const payload = {
                        firstName,
                        lastName,
                        age,
                        photo: e
                    };
        
                    const requestOptions = {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(payload),
                        signal
                    };
                    const response = await fetch('https://simple-contact-crud.herokuapp.com/contact', requestOptions);
                    if(!response.ok){
                        this.setState({ uploadSuccess: "Something went wrong." });
                        this.clearForm();
                        return;
                    }
                    const data = await response.json();
                    const result = data || [];
                    this.setState({
                        uploadSuccess: result.message
                    });
                });
            } else {
                const payload = {
                    firstName,
                    lastName,
                    age
                };

                const requestOptions = {
                    method: 'PUT',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(payload),
                    signal
                };
                const response = await fetch('https://simple-contact-crud.herokuapp.com/contact', requestOptions);
                if(!response.ok){
                    this.setState({ uploadSuccess: "Something went wrong." });
                    this.clearForm();
                    return;
                }
                const data = await response.json();
                const result = data || [];
                this.setState({
                    uploadSuccess: result.message
                });
            }
        }
    }
  }

  render() {
    const {
        style,
        firstName,
        lastName,
        age,
        uploadSuccess,
        isLoading,
        message
    } = this.state;
    return (
      <Layout title="Contact App">
        {
          isLoading ?
            <LoadingIndicator className={style['loading-screen']}/>
          : message ?
            <div className={style['loading-screen']}>
              <font color="red">{message}</font>
            </div>
          :
            <div className={style['contact-container']}>
              <div className={style['contact-header']}>
                <h1>Edit</h1>
                <a href="/">Back</a>
              </div>
                {
                    uploadSuccess ? <div className={style['alert-success']}>{uploadSuccess}</div> : null
                }
              <div className={style['contact-form']}>
                    <FormInput style={style} type={"text"} title={"First Name"} value={firstName} onChange={(e) => this.setState({ firstName: e.target.value })}/>
                    <FormInput style={style} type={"text"} title={"Last Name"} value={lastName} onChange={(e) => this.setState({ lastName: e.target.value })}/>
                    <FormInput style={style} type={"text"} title={"Age"} value={age.toString()} onChange={(e) => this.setState({ age: e.target.value })}/>
                    <FormInput style={style} type={"file"} title={"Upload"} value={null} onChange={(e) => this.setState({ upload: e.target.files })}/>
                    <button onClick={this.handleSubmit} type="submit">Save</button>
                </div>
            </div>
        }
      </Layout>
    )
  }
}

FormEdit.propTypes = {
    setProfile: PropTypes.func,
    clearProfile: PropTypes.func,
    profile: PropTypes.object
};

FormEdit.defaultProps = {
    setProfile: {},
    clearProfile: {},
    profile: {}
};

const mapStateToProps = (state) => {
  const { auth } = state;

  return {
    profile: auth.profile
  };
};

export default connect(mapStateToProps, { setProfile, clearProfile })(FormEdit);