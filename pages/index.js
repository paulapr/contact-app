import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';
import { connect } from 'react-redux';
import Layout from '../src/components/Layout';
import LoadingIndicator from '../src/components/LoadingIndicator';
import { getContact, removeContact, setProfile } from '../redux/actions';
import Swal from 'sweetalert2/dist/sweetalert2';
import styles from '../src/css/style.scss';

class Home extends PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      style: {},
      message: null,
      isLoading: true
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchController = new AbortController();
    this.setState({ style: styles });
    this.handleGetContact(this.fetchController.signal);
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.fetchController.abort();
    this.props.removeContact();
  }

  componentDidUpdate(prevProps, prevState) {
    const { profile } = this.props;
    console.log(profile);
  }

  handleGetContact = async (signal) => {
    if(!this._isMounted){
      return;
    }

    const response = await fetch(
      "https://simple-contact-crud.herokuapp.com/contact",
      { signal },
    );
    
    if(!response.ok){
      this.setState({
        message: 'Something went wrong',
        isLoading: false
      })
      return;
    }

    const data = await response.json();
    const result = data || [];
    await this.props.getContact(result.data).then(() => {
      this.setState({
        isLoading: false
      });
    });
  }

  handleDelete = async (e, id) => {
    if(!this._isMounted){
      return;
    }

    const signal = this.fetchController.signal;
    e.preventDefault();

    Swal.fire({
      title: 'Are you sure want to delete this data ?',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      confirmButtonColor: '#009ddc',
      cancelButtonText: 'No',
      cancelButtonColor: '#009ddc'
    }).then(async (result) => {
        if(result.dismiss === 'cancel' || result.dismiss === 'backdrop') {
          console.log(result.dismiss);
        }
        if(result.value){
          this.setState({ isLoading: true });

          const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
            signal
          };
          const response = await fetch('https://simple-contact-crud.herokuapp.com/contact/'+id, requestOptions);
          if(!response.ok){
            this.setState({
              message: 'Something went wrong',
              isLoading: false
            })
            return;
          }
      
          this.handleGetContact(signal);
        }
    });
  }

  handleEdit = async (id) => {
    await this.props.setProfile(id).then(async () => {
      localStorage.setItem('id', JSON.stringify(id));
      window.location.href = '/form/edit';
    });
  }

  render() {
    const {
      style,
      message,
      isLoading
    } = this.state;
    const { contact } = this.props;
    return (
      <Layout title="Contact App">
        {
          isLoading ?
            <LoadingIndicator className={style['loading-screen']}/>
          : message ?
            <div className={style['loading-screen']}>
              <font color="red">{message}</font>
            </div>
          :
            <div className={style['contact-container']}>
              <div className={style['contact-header']}>
                <h1>Contact</h1>
                <a href="/form/create">Tambah</a>
              </div>
              <div className={style['list-contact']}>
                {
                  contact.map((value, index) => {
                    return(
                      <div key={value.id} className={style['display-name']}>
                        <div className={style['show-image']}>
                          <img alt="profile" src={value.photo} onError={(e) => {e.target.onerror = null; e.target.src = './jenius.png'}} />
                        </div>
                        <div className={style['show-name']}>
                          {value.firstName+" "+value.lastName}
                          <div className={style['handle-btn']}>
                            <button type="submit" onClick={() => this.handleEdit(value.id)}>Edit</button>
                            <button type="submit" onClick={(e) => this.handleDelete(e, value.id)}>Hapus</button>
                          </div>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
        }
      </Layout>
    )
  }
}

Home.propTypes = {
  getContact: PropTypes.func,
  removeContact: PropTypes.func,
  setProfile: PropTypes.func,
  contact: PropTypes.array
};

Home.defaultProps = {
  getContact: {},
  removeContact: {},
  setProfile: {},
  contact: []
};

const mapStateToProps = (state) => {
  const { auth } = state;

  return {
    contact: auth.contact,
    profile: auth.profile
  };
};

export default connect(mapStateToProps, { getContact, removeContact, setProfile })(Home);