const withSass = require('@zeit/next-sass');
const withCss = require('@zeit/next-css');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: process.env.ANALYZE === 'true',
});

const nextConfig = {
    experimental: {
        css: true,
    },
    cssModules: true,
    cssLoaderOptions: {
        importLoaders: 2,
        localIdentName: '[local]___[hash:base64:5]',
    },
    webpack: (config, { dev }) => {
        config.module.rules.forEach((rule) => {
            if (rule.test && rule.test.toString().includes('.scss')) {
                rule.rules = rule.use.map((useRule) => {
                    if (typeof useRule === 'string') {
                        return { loader: useRule };
                    }
                    if (useRule.loader === 'css-loader') {
                        return {
                            oneOf: [
                                {
                                    test: new RegExp('.global.scss$'),
                                    loader: useRule.loader,
                                    options: {},
                                },
                                {
                                    loader: useRule.loader,
                                    options: { modules: true },
                                }
                            ]
                        };
                    }
                    return useRule;
                });
                delete rule.use;
            }
        });
        return config;      
    }    
};

module.exports = withBundleAnalyzer(
    withSass(
        withCss(
            nextConfig
        )
    )
);
  