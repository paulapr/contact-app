import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { withRouter } from 'next/router';

class Layout extends PureComponent {
  render() {
    const {
      children, style, title, router, ...rest
    } = this.props;

    return (
      <div id="body" {...rest}>
        <Head>
          <title>{title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <div className="body-content">
          {children}
        </div>
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  router: PropTypes.object,
};

Layout.defaultProps = {
  children: null,
  title: '',
  router: {},
};


export default withRouter(Layout);