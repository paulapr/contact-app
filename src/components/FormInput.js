import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';

class FormInput extends PureComponent {
  render() {
    const { style, type, title, value, onChange } = this.props;
    return (
        <div className={style['form-input']}>
            <label>{title}</label>
            {type === 'text' ?
              <input type="text" value={value} placeholder={title} onChange={onChange}/>
            :
             <input type="file" onChange={onChange}/> 
            }
        </div>
    )
  }
}

FormInput.propTypes = {
  style: PropTypes.object,
  type: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};

FormInput.defaultProps = {
  style: {},
  type: null,
  title: null,
  value: null,
  onChange: {}
};

export default FormInput;