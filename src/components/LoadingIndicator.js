import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-activity/lib/Spinner';

class LoadingIndicator extends PureComponent {
  render() {
    const { className } = this.props;
    return (
      <div className={className}>
        <Spinner />
      </div>
    );
  }
}

LoadingIndicator.propTypes = {
  className: PropTypes.string,
};

LoadingIndicator.defaultProps = {
  className: '',
};

export default LoadingIndicator;
