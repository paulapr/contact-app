import { combineReducers } from 'redux';
import { GET_CONTACT, REMOVE_CONTACT, PROFILE_DATA, CLEAR_PROFILE } from '../store/types';

const initialState = {
    contact: null,
    profile: null
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CONTACT:
        return {
            ...state,
            contact: action.payload.contact
        };
    case REMOVE_CONTACT:
        return {
            ...state,
            contact: null
        };
    case PROFILE_DATA:
        return {
            ...state,
            profile: action.payload.profile
        };
    case CLEAR_PROFILE:
        return {
            ...state,
            profile: null
        };
    default:
      return state;
  }
};

export default combineReducers({
    auth: authReducer
});
