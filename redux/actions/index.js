import { GET_CONTACT, REMOVE_CONTACT, PROFILE_DATA, CLEAR_PROFILE } from '../store/types';

const getProfile = (profile) => {
  return {
    type: PROFILE_DATA,
    payload: profile
  }
};

const removeProfile = () => {
  return {
    type: CLEAR_PROFILE
  }
};

const setContact = (contact) => {
    return {
      type: GET_CONTACT,
      payload: contact
    }
};

const deleteContact = () => {
  return {
    type: REMOVE_CONTACT
  }
};

export const getContact = (contact) => async (dispatch) => {
  const payload = {
    contact
  };

  return dispatch(setContact(payload));
}

export const removeContact = () => async (dispatch) => {
  dispatch(deleteContact());

  return Promise.resolve();
};

export const setProfile = (profile) => async (dispatch) => {
  const payload = {
    profile
  };

  return dispatch(getProfile(payload));
}

export const clearProfile = () => async (dispatch) => {
  dispatch(removeProfile());

  return Promise.resolve();
};