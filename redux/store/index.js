import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import Reducers from '../reducers';

export const makeStore = (initialState = {}) => createStore(
  Reducers,
  initialState,
  composeWithDevTools(applyMiddleware(thunkMiddleware)),
);